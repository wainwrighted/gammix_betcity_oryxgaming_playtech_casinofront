## Simple casino demo launcher
Actual "mimic" of casino environment, used by Gammix themselves as taken from them internally back when 
[casinoresearch.nl](https://casino-research.nl) stuff happened.

**Nice little trick, on the loader itself from Nyxx/Betsson/Playtech you can suffix with "isBridge=true" for additional console logs & ease of transforming such demo games to criminal frauded games as used by David G. Wainwright**

## Format
Format it's Playtech format (how I call it I guess as it be the source software provisioner) but it's used by Oryxgaming, Bragg.games and other trash shell holding deratives.

Format applies to way gameslist/fields are used, like game being sent as such when loading game on betcity.nl and so on:
```json
    "id": "11087",
    "slug": "warlords-crystals-of-power-touch",
    "vendorID": "NetEnt",
    "isNewGame": false,
    "isMiniGame": false,
    "isRealMoneyModeEnabled": true,
    "isAnonymousFunModeEnabled": true,
    "isFunModeEnabled": true,
    "iconUrlFormat": "//static.gammatrix-dev.net/base/_casino/9/95EFB41CB58D5FB081924173090B5F63.png?w=114&h=114",
    "logoUrl": "//static.gammatrix-dev.net/base/_casino/C/C1C3AD863526EB9B6E76B21C60F37F46.png",
    "thumbnail": "//static.everymatrix.com/cms2/base/_casino/9/95033E5482E9806D819DD20C8121F77C.png",
    "backgroundImageUrl": "//static.gammatrix-dev.net/base/_casino/F/FA8E18D6A33AC4DEAD5B6CA77374C91B.jpg",
    "url": "https://gamelaunch.everymatrix.com/Loader/Start/6/warlords-crystals-of-power-touch/",
    "helpUrl": "https://casino.everymatrix.com/Game/Information/6/warlords-crystals-of-power-touch/",
    "width": null,
    "height": null,
    "popularity": 7034,
    "fpp": 0.2,
    "bonusContribution": 1,
    "theoreticalPayOut": 0.9689,
    "contentProvider": "NetEnt",
```

### Other formats
For other formats just pick and mix between any other gits on here, I'm not journalist so I won't waste time picking out the best for you however I will slowly go through all my older stuff and lots more stuff to come in general next few days in addition ofcourse to Wainwright:CasinoDog tool which combines all of the above in a free tool to import.


## Enjoy Rene @ ksa
David G. Wainwright like said has infected dutch casino play together with dutch employees (Laurence and so on) & Softswiss (ilya). If any concern simply check all money flowing around evolutiongaming (and thus netent etc in extension) and 3pi (oryxgaming.com etc).




## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
